# Cloud & IaaS Basics Project | NodeJS App
We will create a remote server using DigitalOcean droplets and deploy a NodeJS application to it. The source code for the application is included, you can build it yourself using the commands but skipping the original cloning steps.

The steps I used here are part of a Bootcamp so the steps are in order that the exercises were given.

# Technologies Used
- DigitalOcean Droplets
- Linux (Ubuntu)
- NodeJS
- NPM
- GitLab

# Project Description
- Setup and configure a server on DigitalOcean
- Create and configure a new Linux user on the Droplet (security best practice!)
- Deploy and run a NodeJS application on the Droplet

# Prerequisites
- Local machine (I will be using Ubuntu, any OS is fine but steps may vary depending on tools/commands used)
	- Your machine should have Git, NPM, NodeJS installed. If not, you can use `apt install` to get all of them.
- A GitLab or other Git repository account that you can create projects for.
	- Configure it with your personal SSH key from your local machine
- DigitalOcean account and ability to create a droplet
	- Configure it with your personal SSH key from your local machine

# Steps for Setup

## Clone Files and Create Custom Git Repository

### GitLab
1. Log into GitLab and create a new project
	- Public Repository
	- No other special configurations

![Created Project Repository](images/m5p-1-created-project-repository.png)

### Local Machine
1. `mkdir ~/repositories/m5p-exercises`
2. `cd ~/repositories/m5p-exercises`
3. `git clone git@gitlab.com:twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises.git`
4. `cd cloud-basics-exercses`
5. `git remote set-url origin git@gitlab.com:devops-public-projects/m5p-nodejs-app-cloud-deploy-project.git`
6. `git remote -v`
	- Make sure our change went through
7. `mkdir images`
	- Will be used for our readme images later (this readme)
8. `git push --set-upstream origin --all`
9. `git push --set-upstream origin --tags`

![Pushed Files Successful](images/m5p-2-uploaded-files-from-local.png)

![Our Repository Has Our Files](images/m5p-3-files-now-in-our-repository.png)

## Package the NodeJS App
1. `npm pack app/`
2. `ls`
	- Will show our packaged/zipped app (It is not compiled)
	- 
![App Packaged Successfully](images/m5p-4-package-nodejs-app-success.png)

## Create New Remote Server

### DigitalOcean Portal
1. Create an account if you don't already have one
2. Create a new project
3. Create a new Droplet
	- Location = closest to you, we'll use NY
	- Ubuntu 22.10 x64
	- Basic Plan
	- Regular (SSD) / $4/month
	- Authentication > SSH
		- Add your **public** SSH key from your local machine here
			- cat ~/.ssh/id_rsa.pub on Linux
			- If you don't have a key you can use the `ssh-keygen` command on Linux
4.  Finish by pressing **Create Droplet**

![Droplet Made](images/m5p-5-droplet-made.png)

## Prepare Server for Node App

### Local Machine
1. `ssh root@droplet_ip`
	- To connect to your remote server, get your droplet_ip from your DigitalOcean droplet menu

### Remote Server

#### Install Necessary Dependencies
We will need NodeJS and NPM to run our app

1. `apt install nodejs`
2. `nodejs -v`
3. `apt install npm`
4. `npm -v`

![NodeJS and NPM Installed Successfully](images/m5p-6-nodejs-npm-installed-successfully.png)
 

#### Create New User to Run the App
Create a new user for the node app to run as. Also grant it sudo privilege and allow direct SSH access for that account.

1. `adduser node-svc`
2. `usermod -aG sudo node-svc`
3. `su node-svc`
4. `mkdir ~/.ssh`
5. `sudo vim ~/.ssh/authorized_keys`
6. Paste the output of `cat ~/.ssh/id_rsa.pub` from your local machine into this file and save it (`:wq`)

## Copy App to Remote Server
1. `scp bootcamp-node-project-1.0.0.tgz node-svc@143.198.114.103:~`
2. `cd app`
3. `scp package.json node-svc@143.198.114.103:~`

![Local Machine Side Success](images/m5p-7-successful-copy.png)

![Remote Server Side Success](images/m5p-8-successful-copy-2.png)

## Run Node App
### Remote Server
As node-svc user
1. `cd ~`
2. `tar -zxvf bootcamp-node-project-1.0.0.tgz`
	- Extract the package
3. `cd package`
4. `npm install`
	- Install dependencies for the application
5. `sudo -u node-svc node server.js &`
6. Verify everything is running with
	- The app tells us it is running and listening on a port but we can also verify with these two commands
	- Process Running: `ps aux | grep node`
	- Network Listening: `netstat -lpnt`

We can see that the application is running and is listening on port 3000. However, our Droplet is not configured to allow connections for that port yet.

![App IS Running Successfully](images/m5p-9-run-app-verify-running.png)

## Access App from Web Browser

### Before Firewall Configuration
1. Navigate to DROPLET_IP:3000 from your web browser
	- There is no installed certificate so you may need to allow the connection

![Firewall is blocking our browser request](images/m5p-10-failure-to-connect.png)

### Configure Firewall
1. DigitalOcean Menu > Droplets > Networking > Firewalls > Edit > Create Firewall
	- Set a name for the firewall that is descriptive
	- Set inbound port 3000 as open to all ipv4 and ipv6 addresses (This is for the application we will be deploying)
	- **Create Firewall**
2. Go to Networking > Firewall > Droplets > Add Droplet
	- Add your new Droplet to this firewall for it to take effect

![Successful Firewall Exemption](images/m5p-11-successful-firewall-exemption.png)
